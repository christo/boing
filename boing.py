#!/usr/bin/python

import sys, time
from math import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

def keyboard(*args):
    if args[0] == '\033':
        sys.exit()

def init():
    global bounce, rot, x, xinc, bounceinc, rotinc, xmin, xmax
    bounce = 90
    rot = 0
    xmin = 3.5
    xmax = 16 - 3.5
    x = xmin + 0.01
    xinc = 2.6
    bounceinc = 90.0
    rotinc = 60.0

    glLineWidth(2)
    glEnable(GL_DEPTH_TEST)
    
    gengrid()
    genball(9, 16)

def gengrid():
    global gridlist
    gridlist = glGenLists(1)
    glNewList (gridlist, GL_COMPILE)

    glBegin(GL_LINES)
    glColor3d(0.63, 0, 0.63)
    for i in range(0,16+1):
        glVertex3f(0, i, 0)
        glVertex3f(16, i, 0)
        glVertex3f(i, 0, 0)
        glVertex3f(i, 16, 0)
    glEnd()
    glEndList ()

def genball(lats, longs):
    sect = 180 / lats
    rsect = radians(sect)
    lsect = 360.0 / longs

    colorl = [[1,1,1], [.94,0,0]]
    cindex = 0

    global balllist
    balllist = glGenLists(1)
    glNewList(balllist, GL_COMPILE)

    glEnable(GL_CULL_FACE)
    glFrontFace(GL_CCW)
    glCullFace(GL_BACK)

    # Top
    topv = [0, 0, 1]
    scale = sin(radians(sect))
    angle = 0.0
    while angle <= 360.0 - lsect:
        glColor3fv(colorl[cindex])
        cindex ^= 1
        glBegin(GL_POLYGON)
        glVertex3fv(topv)
        glVertex3f(cos(radians(angle)) * scale,
                   sin(radians(angle)) * scale,
                   cos(rsect))
        glVertex3f(cos(radians(angle+lsect)) * scale,
                   sin(radians(angle+lsect)) * scale,
                   cos(rsect))
        glEnd()
        angle += lsect

    # Body of quad strips
    cindex ^= 1
    for step in range(1,lats-1):
        zu = cos(radians(sect * step))
        zl = cos(radians(sect * (step + 1)))
        scaleu = sin(rsect * step)
        scalel = sin(rsect * (step+ 1))
        angle = 0.0
        glBegin(GL_QUADS)
        while angle <= 360.0:            
            cup = cos(radians(angle))
            sup = sin(radians(angle))
            cup2 = cos(radians(angle + lsect))
            sup2 = sin(radians(angle + lsect))
            glColor3fv(colorl[cindex])
            cindex ^= 1
            glVertex3f(cup * scaleu, sup * scaleu, zu)
            glVertex3f(cup * scalel, sup * scalel, zl)
            glVertex3f(cup2 * scalel, sup2 * scalel, zl)
            glVertex3f(cup2 * scaleu, sup2 * scaleu, zu)
            angle += lsect
     
        glEnd()

    # Bottom
    cindex ^= 1
    bv = [0, 0, -1]
    scale = sin(rsect * (lats-1))
    angle = 360.0
    while angle >= 0.0:
        glBegin(GL_POLYGON)
        glColor3fv(colorl[cindex])
        cindex ^= 1
        glVertex3fv(bv)
        glVertex3f(cos(radians(angle)) * scale,
                   sin(radians(angle)) * scale,
                   cos(rsect * (lats - 1)))
        glVertex3f(cos(radians(angle-lsect)) * scale,
                   sin(radians(angle-lsect)) * scale,
                   cos(rsect * (lats - 1)))
        glEnd()
        angle -= lsect    

    glDisable(GL_CULL_FACE)    

    glEndList()

def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode (GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, w/h, 1, 25.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def drawgrid():
    glPushMatrix()
    
    glCallList(gridlist)
    glTranslatef(0, 16, 0)
    glRotatef(90,1,0,0)
    glCallList(gridlist)

    glPopMatrix()

def drawball():
    global bounce, rot, x, bounceinc, rotinc, xinc, now

    last = now
    now = time.time()
    inc =  now - last
    bounce += bounceinc * inc 
    if bounce > 180: bounce = 0
    rot += rotinc * inc
    if rot >= 360: rot = 0
    x += xinc * inc
    if x <= xmin or x >= xmax: xinc = -xinc
    
    glPushMatrix()

    glTranslatef(x, 8 + 4.5, 7 * sin(radians(bounce)) + 3.5)
    glRotatef(22, 0, 1, 0)
    glRotatef(rot, 0, 0, 1)
    glColor3f(1,1,1)
    glScalef(3.5, 3.5, 3.5)
    glPolygonMode(GL_FRONT, GL_FILL)
    glCallList(balllist)

    glPopMatrix()

def display():
    glDrawBuffer(GL_BACK)
    glClearColor(0.63, 0.63, 0.63, 1)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    gluLookAt (8, -6, 8,
               8, 4, 8,
               0, 0, 1)

    drawball()
    drawgrid()

    glutSwapBuffers()

def timer():
    glutPostRedisplay()

def main():
    glutInit(sys.argv)

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL)
    glutInitWindowSize(480, 480)
    glutCreateWindow("Test Window")

    init()

    glutKeyboardFunc(keyboard)
    glutReshapeFunc(reshape)
    glutDisplayFunc(display)
    glutIdleFunc(timer)

    global now
    now = time.time()
    glutMainLoop()
   
main()
